Vue.component("message", {
    template: '<p>hello this is the message from the component</p>'
});



var app = new Vue({
    el: '#app',
    data: {
        name: 'hello',
        image: 'https://www.pinclipart.com/picdir/big/97-973505_resume-writing-skills-full-stack-developer-png-clipart.png',
        height: '200px',
        width: '300px',
        stock: 0,
        link: 'https://www.google.com/',
        employee: ['kishan', 'kumar', 'maharana', 'shubham', 'shee', 'mehdi', 'rizvi'],
        variants: [{ id: 101, color: "green", img: "https://2e8ram2s1li74atce18qz5y1-wpengine.netdna-ssl.com/wp-content/uploads/2018/03/Hipster-Developer-Dice-1024x640.jpg" }, { id: 102, color: "yellow", img: "https://www.arihantwebtech.com/images/design-notice/Hire-Full-Time-Developer.png" }],
        cart: 0,
    },
    methods: {
        changeImage: function(chimg) {
            this.image = chimg;
        },
        increamentCart: function() {
            this.cart++;
        }
    }
});